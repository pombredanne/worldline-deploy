(function(){
jQuery.timeago.settings.strings = {
  prefixAgo: null,
  prefixFromNow: null,
  suffixAgo: "前",
  suffixFromNow: "後",
  seconds: "数秒",
  minute: "1分",
  minutes: "%d分",
  hour: "1時間",
  hours: "%d時間",
  day: "1日",
  days: "%d日",
  month: "1ヶ月",
  months: "%dヶ月",
  year: "1年",
  years: "%d年",
  wordSeparator: "",
  numbers: []
};

function htmlunescape(s){
	return s
		.split('&lt;').join('<')
		.split('&gt;').join('>')
		.split('&quot;').join('"')
		.split('&#39;').join("'")
		.split('&amp;').join('&');
}

function htmlescape(s){
	return s
		.split('&').join('&amp;')
		.split('<').join('&lt;')
		.split('>').join('&gt;')
		.split('"').join('&quot;')
		.split("'").join('&#39;');
}

function autolink(s){
	var re = /(https?|ftp):\/\/[\x21\x23-\x26\x28-\x3b\x3d\x3f-\x7e]+/gi;
	var match, result = "", pos = 0, url;
	while ((match = re.exec(s)) != null){
		url = htmlescape(match[0]);
		result += htmlescape(s.substring(pos, match.index));
		result += '<a target="_blank" href="' + url + '">' + url + '</a>';
		pos = re.lastIndex;
	}
	result += htmlescape(s.substring(pos));
	return result;
}

function br2nl(str) {
	return str.replace(/<br>(\r\n|\n\r|\r|\n)?/ig, "\r\n");
}

function nl2br(str) {
	return str.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2');
}

function zeroPad(num){
	return (num < 10 ? '0' : '') + num;
}

function date2iso8601(d) {
	return d.getUTCFullYear() + '-' + zeroPad(d.getUTCMonth()+1) + '-' + zeroPad(d.getUTCDate()) +
		'T' + zeroPad(d.getUTCHours()) + ':' + zeroPad(d.getUTCMinutes()) + ':' + zeroPad(d.getUTCSeconds()) + 'Z';
}

function blendColor(from, to, step){
	var a = Math.round(step * 255);
	var r = ((from.r * (255 - a) + to.r * a) * 32897) >> 23,
		g = ((from.g * (255 - a) + to.g * a) * 32897) >> 23,
		b = ((from.b * (255 - a) + to.b * a) * 32897) >> 23;
	return 'rgb(' + r + ',' + g + ',' + b + ')';
}

$(function(){
	var postTemplate = $('#post-template').html();
	var nodeTemplate = $('#node-template').html();
	var busy = false;

	var latestModDate = 0;
	$('li[data-moddate]').each(function(){
		var d = parseInt(this.getAttribute('data-moddate'), 10);
		if (d > latestModDate) latestModDate = d;
	});

	function changeBusy(flag){
		var $post = $('#post');
		$post.find(':input')[flag ? 'attr' : 'removeAttr']('readonly', 'readonly');
		busy = flag;
	}

	function initPostContainer($postContainer){
		$postContainer
			.find(':input[name=title]')
				.keydown(function(e){
					if (e.keyCode == 13 && (e.shiftKey || e.ctrlKey)) {
						$(this).closest('form').submit();
						return false;
					}
				})
				.focus();
		$postContainer
			.find('form').submit(function(){
				if ($(':input[name=title]').val() == '') return false;
				if (busy) return false;
				$postContainer.find('iframe').unbind('load error').load(function(){
					var text = $(this.contentWindow.document.body).text();
					$(this).unbind('load error');
					this.contentWindow.location.href = "about:blank";
					try{
						var json = JSON.parse(text);
						if (json.error){
							alert(json.error);
							changeBusy(false);
							return false;
						}
						insertNode(json, true);
					}catch(e){
						alert('エラーが発生したため投稿できませんでした。\n'+text);
						changeBusy(false);
						return false;
					}
					$postContainer.slideUp(500, function(){
						$postContainer.remove();
						changeBusy(false);
					});
				});
				changeBusy(true);
			});
	}

	function writeRes(){
		if (!user || !user.token){
			alert('レスを書くにはログインする必要があります');
			return false;
		}
		var $this = $(this).closest('.node');
		if ($this.find('> div.interface #post').length){
			$('#post').remove();
			return false;
		}
		$('#post').remove();
		var data = $this.data();
		if (user && user.token){
			data.token = user.token;
		}
		var postContainer = $('<div id="post">').html(Mustache.render(postTemplate, data)).appendTo($this.find('> div.interface:eq(0)'));
		initPostContainer(postContainer);
		return false;
	}

	function removePost(){
		if (!user || !user.token){
			alert('書き込みを削除するにはログインする必要があります');
			return false;
		}
		var $this = $(this).closest('.node'), nodeContainer = $this.find('> div.node-container');
		var data = $this.data();

		if (!confirm('以下の書き込みを削除してもよろしいですか？\n\n'+nodeContainer.find('div.title').text())){
			return false;
		}

		$.post('/', {a: 'remove', id: data.id, tk: user.token}, function(json){
			if (json && json.error){
				alert(json.error);
				return;
			}

			var params = {
				node: {
					_id: data.id,
					del: new Date().getTime(),
					title: '（削除）',
					adddate: parseInt(data.adddate, 10),
					moddate: parseInt(data.moddate, 10)
				}
			};
			params.node_adddate_iso8601 = date2iso8601(new Date(params.node.adddate));
			params.node_moddate_iso8601 = date2iso8601(new Date(params.node.moddate));

			nodeContainer.empty().append($(Mustache.render(nodeTemplate, params)).find('div.node-container > *'));
			initNode($this.addClass('node-removed'), true);
		});
		return false;
	}

	function updateTimeago(e){
		$(e ? e : 'span.time').each(function(){
			var $this = $(this);
			var min = ((new Date().getTime()) - $.timeago.datetime($this).getTime()) / (1000*60);
			var c;
			//赤 → 30分 → オレンジ → 30分 → 青 → 5時間 → 薄い青
			if (min < 30){
				c = blendColor({r: 255, g:   0, b:   0}, {r: 255, g: 128, b:   0}, min/30);
			} else if (min < 60) {
				c = blendColor({r: 255, g: 128, b:   0}, {r:   0, g:   0, b: 255}, (min-30)/30);
			} else {
				c = blendColor({r:   0, g:   0, b: 255}, {r: 192, g: 192, b: 255}, Math.min(1, (min-60)/(60*5)));
			}
			$this.css('color', c).text($.timeago.inWords(min*1000*60));
		});
	}

	function cancelNewNodeHighlight(){
		$(this).removeClass('node-new').off('mouseenter', cancelNewNodeHighlight);
	}
	function initNode(elem, omitTextProcessing){
		var $elem = $(elem).find('div.node-container:eq(0)');

		//URLリンク
		var $title = $elem.find('div.title');
		if ($title.length){
			$title.html(nl2br(autolink(htmlunescape(br2nl($title.html())))));
		}

		//レス機能
		$elem.find('a.write-res').click(writeRes);

		//削除機能
		$elem.find('a.remove-post').click(removePost);

		//画像拡大
		$elem.find('div.file a').hover(function(){
			var $this = $(this), zidx = parseInt($this.css('zIndex'), 10);
			$this.css('zIndex', zidx+2);
			$(this).closest('div.file').append('<img class="full" src="'+this.href+'" style="z-index: '+(zidx+1)+'">');
		}, function(){
			var $this = $(this), zidx = parseInt($this.css('zIndex'), 10);
			$this.css('zIndex', zidx-2);
			$(this).closest('div.file').find('img.full').remove();
		});

		updateTimeago($elem.find('span.time'));
	}

	function insertNode(node, local){
		if ($('#n'+node._id).length) return;
		var $parent = $('#n'+(node.parent ? node.parent : ''));
		if (!$parent.length || $parent.hasClass('node-omit')) return;

		var params = {node: node};
		params.node_title_decorated = nl2br(node.title);
		node.adddate = parseInt(node.adddate, 10);
		node.moddate = parseInt(node.moddate, 10);
		params.node_adddate_iso8601 = date2iso8601(new Date(node.adddate));
		params.node_moddate_iso8601 = date2iso8601(new Date(node.moddate));
		params.omit = false;
		params.current_user = user;
		params.current_user_post = node.user && user && node.user._id == user._id;
		if (params.node.user.avatar == 0){
			delete params.node.user.avatar;
		}

		var $elem = $(Mustache.render(nodeTemplate, params));
		$elem[$parent.parent('.tree').length ? 'prependTo' : 'appendTo']($parent.find('ul:eq(0)'));
		initNode($elem, true);
		if (!local) $elem.addClass('node-new').on('mouseenter', cancelNewNodeHighlight);
		$elem.hide().slideDown(500);
	}

	function getLatestAddDate(){
		var o = 0;
		$('[data-adddate]').each(function(){
			o = Math.max(o, parseInt(this.getAttribute('data-adddate'), 10));
		});
		return o;
	}

	function initWebSocket(){
		var ws = new WebSocket((location.protocol == "https:" ? "wss:" : "ws:") + "//" + location.host + ":57102/");
		ws.onopen = function(){
			var id = [];
			//最初のページじゃなければワールドへのノード追加はスルー
			$((logMode ? 'ul.tree li ul ' : '') + 'li.node-full[data-id]').each(function(){
				id.push(this.getAttribute('data-id'));
			});
			var s = JSON.stringify({id:id, o:getLatestAddDate()});
			ws.send(s);
		}
		ws.onmessage = function(e){
			insertNode(JSON.parse(e.data));
		}
		ws.onclose = function(){
			setTimeout(initWebSocket, 1000 * 5);
		}
	}

	function initAjaxReceiver(){
		var o = getLatestAddDate();
		var waitList = [], timer = null;
		function runTimer(){
			var worldId = $('ul.tree li').data('id');
			while(waitList.length){
				node = waitList.shift()
				//最初のページじゃなければワールドへのノード追加はスルー
				if (logMode && worldId == node.parent) continue;
				insertNode(node);
				break;
			}
			if (waitList.length){
				timer = setTimeout(runTimer, 1000);
				return;
			}
			timer = null;
		}
		function receive(){
			$.getJSON(location.protocol + "//" + location.host + ":57102/ax?o="+o, function(nodes){
				$.each(nodes, function(_, node){
					o = Math.max(o, node.adddate);
				});
				Array.prototype.push.apply(waitList, nodes);
				if (!timer) runTimer();
			});
		}
		setInterval(receive, 1000 * 30);
	}

	if ('WebSocket' in window){
		initWebSocket();
	} else {
		initAjaxReceiver();
	}

	$('li.node').each(function(){
		initNode(this);
	});
	setInterval(updateTimeago, 1000 * 60);

	//書き込みやすさの改善にならないか？
	if (user){
		$('li.node-root > div.node-container a.write-res').click();
	}

	$('a.more').on('click', function(){
		var $amore = $(this), params = {world: $amore.data('world'), o: $amore.data('o')};
		if ($amore.hasClass('disabled')) return false;

		$.get('/', params, function(text){
			var html = $(text), parent = $('li.node-root > ul'), moddate;
			html.each(function(){
				moddate = this.getAttribute('data-moddate');
				parent.append(this);
				initNode(this);
				$(this).find('li.node').each(function(){
					initNode(this);
				});
			});
			if (moddate){
				$amore.attr({
					'href': '/?' + (params.world ? 'world=' + params.world + '&' : '') + 'o=' + moddate,
					'data-o': moddate
				}).data('o', moddate);
			} else {
				$amore.addClass('disabled').text('NO MORE');
			}
		}, 'text');
		return false;
	});

	$('.change-account-setting').click(function(){
		var dialog = $('<div class="modal">').html(Mustache.render($('#user-setting-dialog').html(), {user:user, token: user.token})).appendTo(document.body).modal({
			backdrop: 'static'
		});
		dialog.find('.btn-primary').click(function(){
			dialog.find('form').submit();
		});
		dialog.find(':input[name=name]').focus();
		return true;
	});
});

})();
